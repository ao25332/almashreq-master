import 'package:flutter/material.dart';
import 'package:maktabeh_app/core/size_config.dart';
import 'package:maktabeh_app/core/style/baseColors.dart';
import 'package:maktabeh_app/ui/common_widget/app_button.dart';
import 'package:maktabeh_app/ui/guide/guide_screen.dart';

class LanguageScreen extends StatefulWidget {
  @override
  _LanguageScreenState createState() => _LanguageScreenState();
}

class _LanguageScreenState extends State<LanguageScreen> {
  var isLargeScreen = false;

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    if (MediaQuery.of(context).size.height > 700) {
      isLargeScreen = true;
    } else {
      isLargeScreen = false;
    }
    return Scaffold(
      body: Column(
        children: [
          Container(
            height: SizeConfig.screenHeight,
            child: Stack(
              children: [
                Image.asset(
                  'assets/image/language.png',
                  width: SizeConfig.screenWidth,
                  height: SizeConfig.screenHeight * 0.7,
                  fit: BoxFit.fill,
                ),
                Align(
                    alignment: Alignment.topCenter,
                    child: Column(
                      children: [
                        SizedBox(height: SizeConfig.screenHeight * 0.15),
                        Image.asset(
                          'assets/image/logo_image.png',
                          height: SizeConfig.screenHeight * 0.3,
                        ),
                      ],
                    )),
                Positioned(
                  height: isLargeScreen
                      ? SizeConfig.screenHeight * 0.27
                      : SizeConfig.screenHeight * 0.33,
                  width: SizeConfig.screenWidth,
                  bottom: 0,
                  child: Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      // mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'يرجى إختيار اللغة المناسبة لك',
                          style: boldStyle.copyWith(fontSize: 14),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: SizeConfig.screenWidth * 0.05),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              toggleButton('English', 1),
                              SizedBox(
                                width: SizeConfig.screenWidth * 0.1,
                              ),
                              toggleButton('العربية', 0)
                            ],
                          ),
                        ),
                        appButton(
                          buttonColor: primaryColor,
                          context: context,
                          text: "أستمرار",
                          textColor: Colors.white,
                          onTap: () =>
                              Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => GuideScreen(),
                          )),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class toggleButton extends StatefulWidget {
  String text;
  int value;

  toggleButton(this.text, this.value);
  @override
  _toggleButtonState createState() => _toggleButtonState();
}

class _toggleButtonState extends State<toggleButton> {
  int _radioValue = 0;

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;

      switch (_radioValue) {
        case 0:
          break;
        case 1:
          break;
        case 2:
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          widget.text,
          style: regStyle.copyWith(fontSize: 12, fontWeight: FontWeight.w500),
        ),
        Radio(
          value: widget.value,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
          activeColor: primaryColor,
        ),
      ],
    );
  }
}
