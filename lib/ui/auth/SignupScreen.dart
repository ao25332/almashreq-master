import 'package:flutter/material.dart';
import 'package:maktabeh_app/core/style/baseColors.dart';
import 'package:maktabeh_app/ui/common_widget/app_button.dart';
import 'compnent/CustomField2.dart';

class SignupScreen extends StatefulWidget {
  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  @override
  Widget build(BuildContext context) {
    var h = MediaQuery.of(context).size.height;
    var w = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: h * 0.35,
              child: Stack(
                children: [
                  Image.asset(
                    'assets/image/signup.png',
                    width: w,
                    // height: SizeConfig.screenHeight * 0.7,
                    fit: BoxFit.fill,
                  ),

                  // Container(
                  //   child: Positioned(
                  //     child: Image.asset(
                  //       "assets/image/book-library.png",
                  //     ),
                  //     top: -h * 0.25,
                  //     right: -150,
                  //     left: -50,
                  //     // bottom: 5,
                  //   ),
                  // ),
                  // Container(
                  //   child: Positioned(
                  //     child: Image.asset(
                  //       "assets/image/book-library.png",
                  //       color: Colors.black.withOpacity(0.2),
                  //     ),
                  //     top: -h * 0.25,
                  //     right: -150,
                  //     left: -50,
                  //   ),
                  // ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Padding(
                      padding: EdgeInsets.only(top: h * 0.05),
                      child: Image.asset(
                        "assets/image/logo_image.png",
                        height: 120,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Directionality(
              textDirection: TextDirection.rtl,
              child: Padding(
                padding: const EdgeInsets.only(
                  top: 0,
                  left: 20,
                  bottom: 20,
                  right: 10,
                ),
                child: Column(
                  children: [
                    Text(
                      "تسجيل جديد",
                      textAlign: TextAlign.center,
                      style: boldStyle.copyWith(fontSize: 18),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    CustomFeild2(
                      hintText: "الاسم كامل",
                      iconPath: "assets/svg/Profile.svg",
                    ),
                    CustomFeild2(
                      hintText: "البريد الالكتروني",
                      iconPath: "assets/svg/email.svg",
                    ),
                    CustomFeild2(
                      hintText: "اسم المستخدم",
                      iconPath: "assets/svg/Profile.svg",
                    ),
                    CustomFeild2(
                      hintText: "رقم الهاتف (اختياري)",
                      iconPath: "assets/svg/mobile.svg",
                    ),
                    CustomFeild2(
                      visab: true,
                      hintText: "كلمة المرور",
                      iconPath: "assets/svg/Lock.svg",
                    ),
                    CustomFeild2(
                      hintText: "الجنس",
                      iconPath: "assets/svg/User.svg",
                    ),
                    CustomFeild2(
                      hintText: "البلد",
                      iconPath: "assets/svg/Location.svg",
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    appButton(
                      buttonColor: primaryColor,
                      context: context,
                      text: "تسجيل جديد",
                      textColor: Colors.white,
                      onTap: () {},
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
