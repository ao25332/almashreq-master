import 'dart:io';
import 'dart:typed_data';

import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:maktabeh_app/core/config/navigatorHelper.dart';
import 'package:maktabeh_app/core/size_config.dart';
import 'package:maktabeh_app/core/style/baseColors.dart';
import 'package:maktabeh_app/ui/book/about_writer.dart';
import 'package:maktabeh_app/ui/book/books_writer.dart';
import 'package:maktabeh_app/ui/book/buy_books.dart';
import 'package:maktabeh_app/ui/book/quotes_screen.dart';
import 'package:maktabeh_app/ui/common_widget/app_button.dart';
import 'package:maktabeh_app/ui/common_widget/local_image.dart';
import 'package:maktabeh_app/ui/common_widget/rate_stars.dart';
import 'package:maktabeh_app/ui/mainScreens/moreBooksPage.dart';
import 'package:maktabeh_app/ui/review/review_screen.dart';

class BookScreen extends StatefulWidget {
  @override
  _BookScreenState createState() => _BookScreenState();
}

class _BookScreenState extends State<BookScreen> {
  bool showAnswer1 = false;
  bool showAnswer2 = false;
  bool showAnswer3 = false;

  bool isFav = false;

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    return Directionality(
        textDirection: TextDirection.rtl,
        child: SafeArea(
          child: Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              elevation: 0,
              backgroundColor: Colors.white,
              leading: InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Padding(
                    padding: EdgeInsets.all(14.0),
                    child: buildLocalImage(
                      'assets/svg/arrow_back.svg',
                    ),
                  )),
              actions: [
                InkWell(
                  onTap: () async {
                    String _msg;
                    StringBuffer _sb = new StringBuffer();
                    setState(() {
                      _sb.write("كتاب اماريتا كتاب جميل حاول تشوفة");
                      _sb.write(
                          " https://books-library.online/files/books-library.online-1572336149-742.jpg \n");

                      _msg = _sb.toString();
                    });

                    if (1 == 1) {
                      var request = await HttpClient().getUrl(Uri.parse(
                          "https://books-library.online/files/books-library.online-1572336149-742.jpg"));
                      var response = await request.close();
                      Uint8List bytes =
                          await consolidateHttpClientResponseBytes(response);
                      await Share.file(
                          'ESYS AMLOG', 'amlog.jpg', bytes, 'image/jpg',
                          text: _msg);
                    } else {
                      Share.text("title", _msg, 'text/plain');
                    }
                  },
                  child: Padding(
                      padding: EdgeInsets.all(14.0),
                      child: Icon(
                        Icons.share,
                        color: seconderyColor,
                        // child: buildLocalImage(
                        //   isFav
                        //       ? 'assets/svg/fav_off.svg'
                        //       : 'assets/svg/fav_on.svg',
                        // ),
                      )),
                ),
                InkWell(
                    onTap: () {
                      setState(() {
                        isFav = !isFav;
                      });
                    },
                    child: Padding(
                      padding: EdgeInsets.all(14.0),
                      child: buildLocalImage(
                        isFav
                            ? 'assets/svg/fav_off.svg'
                            : 'assets/svg/fav_on.svg',
                      ),
                    )),
              ],
            ),
            body: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.blockSizeHorizontal * 4),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/image/book_image.png',
                    ),
                    SizedBox(height: SizeConfig.blockSizeVertical),
                    Column(
                      children: [
                        Text(
                          'الأوابد',
                          style: boldStyle.copyWith(fontSize: 18),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: SizeConfig.blockSizeVertical),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/icons/Profile.png',
                                height: 12,
                                color: primaryColor,
                              ),
                              SizedBox(width: 4),
                              Text(
                                'عبدالوهاب عزام',
                                style: lightStyle.copyWith(
                                    color: Color(0xFFABABAB), fontSize: 12),
                              )
                            ],
                          ),
                        ),
                        rateStars(20),
                        Divider(
                          thickness: 1,
                          color: Color(0xFFE5E5E5),
                        ),
                        appButton(
                          context: context,
                          onTap: () {
                            showDialog(
                                context: context,
                                builder: (BuildContext ctx) {
                                  return alertDialog(ctx);
                                });
                          },
                          text: 'قراءة الكتاب ',
                        ),
                        Divider(
                          thickness: 1,
                          color: Color(0xFFE5E5E5),
                        ),
                        bookRow(
                            show: showAnswer1,
                            icon: 'assets/svg/book.svg',
                            arrow: 'assets/svg/arrow_down.svg',
                            widget: bookInfo(),
                            onTab: () {
                              setState(() {
                                showAnswer1 = !showAnswer1;
                              });
                            },
                            text: 'معلومات عن الكتاب'),
                        Divider(
                          thickness: 1,
                          color: Color(0xFFE5E5E5),
                        ),
                        bookRow(
                            show: showAnswer3,
                            icon: 'assets/svg/star.svg',
                            widget: Container(),
                            onTab: () =>
                                Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => ReviewScreen(),
                                )),
                            text: 'المراجعات'),
                        Divider(
                          thickness: 1,
                          color: Color(0xFFE5E5E5),
                        ),
                        bookRow(
                            show: showAnswer3,
                            icon: 'assets/svg/buy.svg',
                            widget: Container(),
                            onTab: () =>
                                Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => BuyBooksScreen(),
                                )),
                            text: 'شراء الكتاب'),
                        Divider(
                          thickness: 1,
                          color: Color(0xFFE5E5E5),
                        ),
                        bookRow(
                            show: showAnswer3,
                            icon: 'assets/svg/quote.svg',
                            widget: Container(),
                            onTab: () =>
                                Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => QuotesScreen(),
                                )),
                            text: 'الإقتباسات'),
                        Divider(
                          thickness: 1,
                          color: Color(0xFFE5E5E5),
                        ),
                        bookRow(
                            show: showAnswer2,
                            icon: 'assets/svg/person.svg',
                            arrow: 'assets/svg/arrow_down.svg',
                            widget: aboutWriter(),
                            onTab: () {
                              setState(() {
                                showAnswer2 = !showAnswer2;
                              });
                            },
                            text: 'عن الكاتب'),
                        Divider(
                          thickness: 1,
                          color: Color(0xFFE5E5E5),
                        ),
                        bookRow(
                            show: showAnswer3,
                            icon: 'assets/svg/book.svg',
                            widget: Container(),
                            onTab: () => push(
                                context,
                                MoreBookPage(
                                  title: "كتب لنفس الكاتب",
                                  bookNum: true,
                                )),
                            text: 'كتب من نفس الكاتب'),
                        Divider(
                          thickness: 1,
                          color: Color(0xFFE5E5E5),
                        ),
                        bookRow(
                            show: showAnswer3,
                            icon: 'assets/svg/book.svg',
                            widget: Container(),
                            onTab: () =>
                                Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => AboutWriterScreen(),
                                )),
                            text: 'كتب من نفس القسم'),
                        Divider(
                          thickness: 1,
                          color: Color(0xFFE5E5E5),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}

Widget bookRow(
    {bool show,
    Function onTab,
    String text,
    String icon,
    String arrow,
    Widget widget}) {
  return InkWell(
    onTap: onTab,
    child: Column(
      children: [
        Container(
          padding: EdgeInsets.symmetric(vertical: SizeConfig.blockSizeVertical),
          child: Row(
            children: [
              Padding(
                padding:
                    EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 2),
                child: buildLocalImage(icon),
              ),
              Text(
                text,
                style: boldStyle,
              ),
              Spacer(),
              GestureDetector(
                child: Container(
                  child: Visibility(
                    visible: show,
                    child: buildLocalImage(
                      'assets/svg/arrow_up.svg',
                    ),
                    replacement: buildLocalImage(
                      arrow ?? 'assets/svg/arrow_forward.svg',
                    ),
                  ),
                ),
                onTap: onTab,
              )
            ],
          ),
        ),
        Visibility(
          visible: show,
          child: widget,
        ),
      ],
    ),
  );
}

Widget alertDialog(BuildContext context) {
  return Dialog(
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 4),
          child: buildLocalImage(
            'assets/svg/dialog.svg',
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: SizeConfig.blockSizeVertical),
          child: Text(
            'لا يمكن قراءة الكتاب',
            style: boldStyle.copyWith(fontSize: 18),
            textAlign: TextAlign.center,
          ),
        ),
        Text(
          'سجل الآن في ثواني.. فجميع الكتب ستكون متاحة بعد التسجيل',
          style: regStyle.copyWith(color: Color(0xFF9C9C9C)),
          textAlign: TextAlign.center,
        ),
        // SizedBox(height: 6),
        // Text(
        //   '.في تطبيق مكتبة المشرق الإلكترونية',
        //   style: regStyle.copyWith(color: Color(0xFF9C9C9C)),
        //   textAlign: TextAlign.center,
        // ),
        Padding(
          padding: EdgeInsets.symmetric(
              vertical: SizeConfig.blockSizeVertical * 2,
              horizontal: SizeConfig.blockSizeHorizontal * 6),
          child: appButton(
            text: 'إشتراك',
            context: context,
            onTap: () {},
          ),
        ),
        InkWell(
          onTap: () {},
          child: Text(
            'إلغاء',
            style: regStyle.copyWith(
                color: Color(0xFF12416D), fontWeight: FontWeight.w500),
          ),
        ),
        SizedBox(height: 20),
      ],
    ),
  );
}

Widget bookInfo() {
  return Column(
    children: [
      Divider(
        thickness: 1,
        color: Color(0xFFE5E5E5),
      ),
      Container(
        color: Colors.white,
        height: SizeConfig.screenHeight * 0.3,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                RichText(
                  text: TextSpan(
                      text: 'القسم: ',
                      style: boldStyle.copyWith(
                        color: primaryColor,
                      ),
                      children: [
                        TextSpan(
                            text: 'روايات عربية',
                            style: regStyle.copyWith(color: Colors.black))
                      ]),
                ),
                RichText(
                  text: TextSpan(
                      text: 'القسم: ',
                      style: boldStyle.copyWith(
                        color: primaryColor,
                      ),
                      children: [
                        TextSpan(
                            text: 'روايات عربية',
                            style: regStyle.copyWith(color: Colors.black))
                      ]),
                ),
              ],
            ),
            Padding(
              padding:
                  EdgeInsets.symmetric(vertical: SizeConfig.blockSizeVertical),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  RichText(
                    text: TextSpan(
                        text: 'القسم: ',
                        style: boldStyle.copyWith(
                          color: primaryColor,
                        ),
                        children: [
                          TextSpan(
                              text: 'روايات عربية',
                              style: regStyle.copyWith(color: Colors.black))
                        ]),
                  ),
                  RichText(
                    text: TextSpan(
                        text: 'القسم: ',
                        style: boldStyle.copyWith(
                          color: primaryColor,
                        ),
                        children: [
                          TextSpan(
                              text: 'روايات عربية',
                              style: regStyle.copyWith(color: Colors.black))
                        ]),
                  ),
                ],
              ),
            ),
            Text(
              'تعريف',
              style: boldStyle.copyWith(color: Colors.black),
            ),
            Container(
              width: SizeConfig.screenWidth,
              child: Text(
                'قبل أيام من اندلاع حرب السادس من أكتوبر لعام 1973 ميلادية، يتم تكليف أربعة من جنود وضباط من الصاعقة المصرية بنسف محطة الرصد والتقصي الإسرائيلية (عاين) والتي تقع في أعماق سيناء المحتلة. كانت تلك العملية غاية في الخطورة وقد يتوقف عليها نجاح أو فشل الضربة الجوية الأولى',
                style: lightStyle.copyWith(fontSize: 12, height: 1.8),
              ),
            )
          ],
        ),
      ),
    ],
  );
}

Widget aboutWriter() {
  return Container(
    height: SizeConfig.screenHeight * 0.5,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Divider(
          thickness: 1,
          color: Color(0xFFE5E5E5),
        ),
        Text(
          'تعريف',
          style: boldStyle.copyWith(color: Colors.black),
        ),
        Row(
          children: [
            Expanded(
              flex: 5,
              child: Text(
                'عبد الوهاب عزام: رائد الدراسات الفارسية، الأديب والدبلوماسي والباحث والمفكر الذي قدَّم للميادين البحثية طائفةً متنوِّعةً من الأبحاث في الأدب والتاريخ والتصوف. وقد اتسم بعُمق الثقافة العربية والإسلامية والأدبية؛ فقد وقف على أدب ',
                style: regStyle.copyWith(fontSize: 12, height: 1.8),
              ),
            ),
            Expanded(
                flex: 2,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.asset(
                    "assets/image/writer.png",
                    fit: BoxFit.fill,
                    height: SizeConfig.screenHeight * 0.2,
                    width: SizeConfig.screenHeight * 0.1,
                  ),
                ))
          ],
        ),
        Text(
          'قبل أيام من اندلاع حرب السادس من أكتوبر لعام 1973 ميلادية، يتم تكليف أربعة من جنود وضباط من الصاعقة المصرية بنسف محطة الرصد والتقصي الإسرائيلية (عاين) والتي تقع في أعماق سيناء المحتلة. كانت تلك العملية غاية في الخطورة وقد يتوقف عليها نجاح أو فشل الضربة الجوية الأولى',
          style: regStyle.copyWith(fontSize: 12, height: 1.8),
        ),
      ],
    ),
  );
}
