import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:maktabeh_app/core/config/navigatorHelper.dart';
import 'package:maktabeh_app/core/style/baseColors.dart';
import 'package:maktabeh_app/ui/user/MyQuote.dart';
import 'package:maktabeh_app/ui/user/SubscriptionScreen/SubscriptionScreen.dart';
import 'package:maktabeh_app/ui/user/editProfile.dart/editProfileScreen.dart';
import 'package:maktabeh_app/ui/user/myReviews.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.all(10),
        children: [
          Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height * 0.15,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(15),
            ),
            padding: EdgeInsets.all(10),
            child: Row(
              children: [
                Container(
                  height: 80,
                  width: 80,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(1000),
                    child: Image.asset(
                      "assets/image/3.jpg",
                      fit: BoxFit.fill,
                      height: double.infinity,
                      width: double.infinity,
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        "عبدالله اسامة",
                        style: regStyle,
                      ),
                      Text(
                        "ao25332@gmail.com",
                        style: regStyle,
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(
                            Icons.location_on_outlined,
                            color: Colors.grey,
                            size: 18,
                          ),
                          Text(
                            "مصر",
                            style: regStyle.copyWith(
                                color: Colors.grey, fontSize: 12),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                Center(
                  child: IconButton(
                    icon: Image.asset("assets/icons/Edit.png"),
                    onPressed: () => push(context, EfitProfileScreen()),
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              optionSetting(
                onTap: () => push(context, SubscriptionScreen()),
                pathIcon: "assets/icons/Iconly-Broken-Discount.png",
                title: "اشتركاتي",
              ),
              optionSetting(
                onTap: () => push(context, MyQuote(title: "اقتباساتي")),
                pathIcon: "assets/icons/Iconly-Broken-Chat.png",
                title: "اقتباساتي",
              ),
              optionSetting(
                onTap: () => push(context, MyReviews()),
                pathIcon: "assets/icons/Iconly-Broken-Star.png",
                title: "مراجعاتي",
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            "الاعدادات العامة",
            style: boldStyle,
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            height: 0.5,
            width: double.infinity,
            color: Colors.grey,
          ),
          ListTile(
            title: Row(
              children: [
                Text(
                  "لغه التطبيق",
                  style: regStyle.copyWith(color: Colors.black),
                ),
                Expanded(child: SizedBox()),
                Text(
                  "العربية",
                  style: regStyle.copyWith(fontSize: 13),
                ),
              ],
            ),
            trailing: SvgPicture.asset("assets/svg/arrow_down.svg"),
            leading: ImageIcon(
              AssetImage("assets/icons/Chat.png"),
              size: 50,
              color: Color(0xFF28ABE3),
            ),
          ),
          Container(
            height: 0.5,
            width: double.infinity,
            color: Colors.grey,
          ),
          ListTile(
            title: Text(
              "تسجيل الخروج",
              style: regStyle.copyWith(color: Colors.black),
            ),
            trailing: SvgPicture.asset("assets/svg/arrow_forward.svg"),
            leading: ImageIcon(
              AssetImage("assets/icons/logout.png"),
              size: 50,
              color: Color(0xFF28ABE3),
            ),
          ),
          Container(
            height: 0.5,
            width: double.infinity,
            color: Colors.grey,
          ),
        ],
      ),
    );
  }

  Widget optionSetting({String title, String pathIcon, Function onTap}) {
    return Expanded(
      child: InkWell(
        onTap: onTap,
        child: Container(
          margin: EdgeInsets.all(5),
          height: 100,
          width: 100,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(15),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                pathIcon,
                height: 50,
                width: 50,
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                title,
                style: regStyle.copyWith(color: seconderyColor),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
