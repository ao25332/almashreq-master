import 'package:flutter/material.dart';
import 'package:maktabeh_app/core/style/baseColors.dart';
import 'package:maktabeh_app/ui/common_widget/CustomNavBar.dart';
import 'package:maktabeh_app/ui/user/MyQuote.dart';
import 'HomSereens/HomeScreen.dart';
import 'NotificationScreen.dart';
import 'OuthorsScreen.dart';
import 'ProfileScreen.dart';
import 'categorisScreen.dart';
import 'drawer/CustomDrawer.dart';
import 'searchScreen.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage>
    with SingleTickerProviderStateMixin {
  var _bottomNavIndex = 0;

  List<Widget> widgets = [
    HomeScreen(),
    CategoriesScreen(),
    OuthorScreen(),
    MyQuote(title: "الاقتباسات"),
    SettingsScreen(),
  ];
  List<String> screenTitle = [
    "الرئيسة",
    "الاقسام",
    "المؤلفين",
    "الاقتباس",
    "حسابي",
  ];

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        drawer: CustomDrawer(),
        appBar: AppBar(
          title: Text(
            screenTitle[_bottomNavIndex],
            style: regStyle.copyWith(
                color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500),
          ),
          centerTitle: true,
          leading: Builder(
            builder: (context) => IconButton(
              icon: ImageIcon(
                AssetImage("assets/icons/Icon_sorting.png"),
                color: Colors.white,
              ),
              onPressed: () => Scaffold.of(context).openDrawer(),
            ),
          ),
          actions: [
            IconButton(
                icon: ImageIcon(
                  AssetImage("assets/icons/Search.png"),
                  color: Colors.white,
                ),
                onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => SearchScreen(),
                    ))),
            IconButton(
                icon: ImageIcon(
                  AssetImage("assets/icons/Notification.png"),
                  color: Colors.white,
                ),
                onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => NotificationScreen(),
                    ))),
          ],
        ),
        extendBody: true,
        body: widgets[_bottomNavIndex],
        bottomNavigationBar: AppBottomBar(
          onTap: (v) {
            setState(() {
              _bottomNavIndex = v;
            });
          },
        ),
      ),
    );
  }
}
