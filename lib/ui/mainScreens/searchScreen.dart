import 'package:flutter/material.dart';
import 'package:maktabeh_app/core/size_config.dart';
import 'package:maktabeh_app/core/style/baseColors.dart';
import 'package:maktabeh_app/ui/common_widget/app_bar.dart';
import 'package:maktabeh_app/ui/common_widget/app_button.dart';
import 'package:maktabeh_app/ui/common_widget/customAppBar.dart';
import 'dart:ui' as ui;

import 'package:maktabeh_app/ui/common_widget/filter_drop_down.dart';
import 'package:maktabeh_app/ui/common_widget/filter_textfield.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  bool isSelected = false;
  List<String> list = [
    'التاريخ الإسلامي',
    'قصص قصيرة',
    'روايات',
    'شعر',
    'مسرحيات مترجمة',
    'التاريخ العربي'
  ];

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: app_bar('البحث', context),
        body: Stack(
          children: [
            ListView(
              padding: EdgeInsets.all(10),
              children: [
                TextFormField(
                  decoration: InputDecoration(
                    hintText: "اكتب كلمة البحث هنا ...",
                    contentPadding: EdgeInsets.all(16),
                    border: OutlineInputBorder(
                      borderSide:
                          BorderSide(width: 0.1, color: Colors.grey[50]),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    hintStyle: regStyle.copyWith(color: Color(0xFFCBCBCB)),
                    prefixIcon: ImageIcon(
                      AssetImage(
                        "assets/icons/Search.png",
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 8.0,
                    right: 15,
                  ),
                  child: Text(
                    "فلتر البحث",
                    style: boldStyle,
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "اسم الكاتب",
                        style: boldStyle,
                      ),
                      SizedBox(
                        width: 25,
                      ),
                      filterDropDown(
                          hint: 'اسم الكاتب',
                          width: SizeConfig.screenWidth * 0.5)
                    ],
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  height: 0.5,
                  width: double.infinity,
                  color: Colors.grey,
                ),
                SizedBox(
                  height: 25,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 8.0,
                    right: 10,
                  ),
                  child: Text(
                    "كلمات مفتاحية",
                    style: boldStyle,
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                Container(
                    child: Wrap(
                  children: List.generate(list.length, (index) {
                    return Container(
                      margin: EdgeInsets.all(5),
                      child: FilterChip(
                        backgroundColor: greyColor,
                        padding: EdgeInsets.symmetric(
                            horizontal: 20,
                            vertical: SizeConfig.blockSizeVertical * 2),
                        label: Text(
                          list[index],
                          style: regStyle.copyWith(
                            fontSize: 12,
                            color:
                                isSelected ? Colors.black : Color(0xFF9C9C9C),
                          ),
                          textAlign: TextAlign.center,
                        ),
                        selected: false,
                        onSelected: (bool selected) {
                          setState(() {
                            isSelected = !isSelected;
                          });
                        },
                        shape: RoundedRectangleBorder(
                          side: isSelected
                              ? BorderSide(
                                  color: primaryColor,
                                  width: 1.0,
                                )
                              : BorderSide(
                                  color: Colors.white,
                                  width: 0,
                                ),
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                      ),
                    );
                  }),
                )),
                SizedBox(
                  height: 30,
                ),
                Container(
                  height: 0.5,
                  width: double.infinity,
                  color: Colors.grey,
                ),
                SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "انبذة عن الكتاب",
                        style: boldStyle,
                      ),
                      SizedBox(
                        width: 25,
                      ),
                      filterTextField(
                          hint: 'نبذة عن الكتاب',
                          width: SizeConfig.screenWidth * 0.5)
                    ],
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  height: 0.5,
                  width: double.infinity,
                  color: Colors.grey,
                ),
                SizedBox(
                  height: 80,
                ),
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: appButton(
                  buttonColor: primaryColor,
                  context: context,
                  onTap: () {},
                  text: "بحث",
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
