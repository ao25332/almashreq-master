import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:maktabeh_app/core/style/baseColors.dart';
import 'package:maktabeh_app/ui/common_widget/outhorCard.dart';

class SupremeWriterPage extends StatefulWidget {
  @override
  _SupremeWriterPageState createState() => _SupremeWriterPageState();
}

class _SupremeWriterPageState extends State<SupremeWriterPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Container(
                      height: 20,
                      width: 3,
                      color: Color(0xFF1A6C9E),
                      margin: const EdgeInsets.symmetric(horizontal: 5),
                    ),
                    Text(
                      "ابرز المؤلفون",
                      style: boldStyle,
                    ),
                  ],
                ),
                InkWell(
                  // onTap: () => push(context, AllReviewsPage()),
                  child: Text(
                    "شاهد المزيد",
                    style: lightStyle.copyWith(fontSize: 10),
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: double.infinity,
            child: GridView.count(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              padding: EdgeInsets.only(right: 5, left: 5),
              childAspectRatio: (1 / 1.8),
              crossAxisCount: 3,
              children: List.generate(6, (index) {
                return OuthorCard();
              }),
            ),
          ),
        ],
      ),
    );
  }
}
