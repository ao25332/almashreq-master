import 'package:flutter/material.dart';
import 'package:maktabeh_app/core/config/navigatorHelper.dart';
import 'package:maktabeh_app/core/style/baseColors.dart';
import 'package:maktabeh_app/ui/common_widget/BookCard.dart';

import '../../moreBooksPage.dart';

class MainList extends StatefulWidget {
  final String title;

  const MainList({Key key, this.title}) : super(key: key);
  @override
  _MainListState createState() => _MainListState();
}

class _MainListState extends State<MainList> {
  @override
  Widget build(BuildContext context) {
    var h = MediaQuery.of(context).size.height;
    var w = MediaQuery.of(context).size.width;
    return Container(
      padding: EdgeInsets.only(top: 10),
      height: h * 0.338,
      width: w,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(right: 8, left: 8, bottom: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Container(
                      height: 20,
                      width: 3,
                      color: Color(0xFF1A6C9E),
                      margin: const EdgeInsets.symmetric(horizontal: 5),
                    ),
                    Text(
                      widget.title,
                      style: boldStyle,
                    ),
                  ],
                ),
                InkWell(
                  onTap: () => push(context, MoreBookPage(title: widget.title)),
                  child: Text(
                    "شاهد المزيد",
                    style: lightStyle.copyWith(fontSize: 10),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
              child: ListView.builder(
            itemCount: 5,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return BookCard();
            },
          ))
        ],
      ),
    );
  }
}
