import 'package:flutter/material.dart';
import 'package:maktabeh_app/ui/mainScreens/HomSereens/Compenant/supremeWriterPage.dart';
import 'Compenant/HomeCategoris.dart';
import 'Compenant/QuoteToday.dart';
import 'Compenant/ReviewToday.dart';
import 'Compenant/mainList.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          // categoris //
          HomeCategoris(),
          // for you
          MainList(
            title: "من اجلك",
          ),
          // all books
          MainList(
            title: "كل الكتب",
          ),
          //most review
          MainList(
            title: "الاكثر مراجعه",
          ),
          // recently add
          MainList(
            title: "اضيف مؤخرا",
          ),
          //QuoteToday
          QuoteToday(),
          // review today
          ReviewToday(),
          // writers
          SupremeWriterPage(),
        ],
      ),
    );
  }
}
