import 'package:flutter/material.dart';
import 'package:maktabeh_app/core/size_config.dart';
import 'package:maktabeh_app/core/style/baseColors.dart';
import 'package:maktabeh_app/ui/common_widget/customAppBar.dart';
import 'package:maktabeh_app/ui/mainScreens/HomSereens/Compenant/QuoteCard.dart';

class MyQuote extends StatefulWidget {
  final String title;

  const MyQuote({Key key, @required this.title}) : super(key: key);
  @override
  _MyQuoteState createState() => _MyQuoteState();
}

class _MyQuoteState extends State<MyQuote> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Text(widget.title, style: boldStyle),
            ),
            Expanded(
              child: ListView.builder(
                padding: EdgeInsets.symmetric(horizontal: 10),
                itemCount: 3,
                itemBuilder: (context, index) {
                  return QuoteCard(title: widget.title);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
