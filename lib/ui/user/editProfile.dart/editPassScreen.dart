import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:maktabeh_app/core/style/baseColors.dart';
import 'package:maktabeh_app/ui/auth/compnent/CustomField2.dart';
import 'package:maktabeh_app/ui/common_widget/app_button.dart';
import 'package:maktabeh_app/ui/common_widget/customAppBar.dart';

class EditPassScreen extends StatefulWidget {
  @override
  _EditPassScreenState createState() => _EditPassScreenState();
}

class _EditPassScreenState extends State<EditPassScreen> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: customAppBar(context, "تغير كلمة المرور"),
        body: ListView(
          padding: EdgeInsets.all(20),
          children: [
            Text(
              "تغير كلمة المرور",
              style: boldStyle,
            ),
            SizedBox(
              height: 30,
            ),
            CustomFeild2(
              title: "كلمة المرور الحالية",
              hintText: "*********",
              headIcon: "assets/icons/Lock.png",
              // iconPath: "assets/svg/Lock.svg",
              visab: true,
            ),
            CustomFeild2(
              title: "كلمة المرور الجديدة",
              hintText: "*********",
              headIcon: "assets/icons/Lock.png",
              // iconPath: "assets/svg/Lock.svg",
              visab: true,
            ),
            CustomFeild2(
              title: "تاكيد كلمة المرور",
              hintText: "*********",
              headIcon: "assets/icons/Lock.png",
              // iconPath: "assets/svg/Lock.svg",
              visab: true,
            ),
            SizedBox(
              height: 70,
            ),
            appButton(
              context: context,
              buttonColor: primaryColor,
              onTap: () {},
              text: "حفظ التغيرات",
            ),
          ],
        ),
      ),
    );
  }
}
