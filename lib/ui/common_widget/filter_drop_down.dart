import 'package:flutter/material.dart';
import 'package:maktabeh_app/core/size_config.dart';
import 'package:maktabeh_app/core/style/baseColors.dart';

import 'local_image.dart';

Widget filterDropDown({String hint, double height, double width}) {
  return Container(
    width: width ?? SizeConfig.screenWidth * 0.4,
    height: height ?? SizeConfig.screenHeight * 0.07,
    padding: EdgeInsets.all(12),
    decoration: ShapeDecoration(
      shape: RoundedRectangleBorder(
        side: BorderSide(width: 1, color: Color(0xFFCBCBCB)),
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
      ),
    ),
    child: DropdownButton(
      onChanged: (String newValue) {},
      // value: 'اختر اسم الكاتب',

      items: [
        DropdownMenuItem<String>(
          child: Text(
            "اخر اسم الكاتب",
            style: lightStyle.copyWith(color: Colors.grey),
          ),
        ),
        DropdownMenuItem<String>(
          child: Text(
            "طه",
            style: lightStyle.copyWith(color: Colors.grey),
          ),
        )
      ],
      underline: Container(),
      isExpanded: true,
      icon: buildLocalImage('assets/svg/arrow_down.svg'),
      hint: Text(hint,
          style: regStyle.copyWith(color: Color(0xFFCBCBCB), fontSize: 12)),
    ),
  );
}
